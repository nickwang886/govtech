<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Handle user access
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function (Request $request) {
        return auth()->user();
    });

    Route::post('/logout', [AuthController::class, 'logout']);
});
//End of handle user access

Route::group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers\Api\v1'], function () {
    Route::apiResource('products', ProductController::class, ['except' => ['destroy']]);
});

// Route::delete('v1/products/{id}', ['uses' => 'App\Http\Controllers\Api\v1\ProductController@delete'])->middleware(['auth:sanctum', 'ability:admin']);
Route::group(['middleware' => ['auth:sanctum', 'ability:admin']], function () {
    Route::delete('v1/products/{id}', ['uses' => 'App\Http\Controllers\Api\v1\ProductController@delete']);
});
