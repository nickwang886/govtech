<?php

namespace App\Queries;

use Illuminate\Http\Request;

class BaseQuery
{
    /**
     * Request parameters
     * @var array
     */
    protected $safeParams = [];

    /**
     * Request name mapped with database column name
     * @var array
     */
    protected $columnMap = [];

    /**
     * Operator name mapped with database operator
     * @var array
     */
    protected $operatorMap = [
        'eq'   => '=',
        'lt'   => '<',
        'lteq' => '<=',
        'gt'   => '>',
        'gteq' => '>=',
        'like' => 'like'
    ];

    /**
     * Convert request paramters into eloquent query
     * @return array
     */
    public function transform(Request $request)
    {
        $arrQueryItem = [];

        foreach ($this->safeParams as $param => $operators) {
            $query = $request->query($param);

            if (!isset($param)) {
                continue;
            }

            $column = $this->columnMap[$param] ?? $param;

            foreach ($operators as $operator) {
                if (isset($query[$operator])) {
                    switch ($this->operatorMap[$operator]) {
                        case 'like':
                            $arrQueryItem[] = [$column, $this->operatorMap[$operator], '%' . $query[$operator] . '%'];
                            break;
                        default:
                            $arrQueryItem[] = [$column, $this->operatorMap[$operator], $query[$operator]];
                            break;
                    }
                }
            }
        }

        return $arrQueryItem;
    }
}
