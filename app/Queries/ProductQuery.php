<?php

namespace App\Queries;

use App\Queries\BaseQuery;

class ProductQuery extends BaseQuery
{
    protected $safeParams = [
        'product_id'          => ['eq', 'like'],
        'product_name'        => ['eq', 'like'],
        'product_description' => ['eq', 'like'],
        'product_link'        => ['eq', 'like'],
        'categories'          => ['eq', 'like'],
        'docs'                => ['like']
    ];
}
