<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'                  => $this->id,
            'product_id'          => $this->product_id,
            'product_name'        => $this->product_name,
            'product_description' => $this->product_description,
            'product_link'        => $this->product_link,
            'categories'          => $this->categories,
            'docs'                => $this->docs
        ];
    }
}
