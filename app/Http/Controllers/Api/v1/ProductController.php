<?php

namespace App\Http\Controllers\Api\v1;

use App\http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Queries\ProductQuery;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ApiResponse;

    public function __construct()
    {
        //
    }

    /**
     * Display all products
     *
     * @return collection
     */
    public function index(Request $request)
    {
        $filter     = new ProductQuery;
        $queryItems = $filter->transform($request); //format: [column, operator, value]

        if ($queryItems) {
            return new ProductCollection(Product::where($queryItems)->paginate());
        }

        return new ProductCollection(Product::all());
    }

    /**
     * Display selected product
     *
     * @return resource
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Delete selected product (Ability: admin)
     *
     * @return response
     */
    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return $this->respondSuccess('product deleted');
    }
}
