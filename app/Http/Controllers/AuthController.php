<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use ApiResponse;

    /**
     * Handle user registration
     *
     * @return App\Traits\ApiResponse
     */
    public function register(Request $request)
    {
        $attr = $request->validate([
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|unique:users,email',
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::create([
            'name'     => $attr['name'],
            'password' => bcrypt($attr['password']),
            'email'    => $attr['email']
        ]);

        return $this->respondSuccess([
            'token' => $user->createToken('basic-token', ['basic'])->plainTextToken
        ]);
    }

    /**
     * Handle user login
     *
     * @return App\Traits\ApiResponse
     */
    public function login(Request $request)
    {
        $attr = $request->validate([
            'email'    => 'required|string|email',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::attempt($attr)) {
            return $this->respondWithError('credentials do not match');
        }

        //Check for admin user
        if (auth()->user()->name === User::ADMIN) {
            return $this->respondSuccess([
                'token-type' => 'admin-token',
                'token'      => auth()->user()->createToken('admin-token', ['admin'])->plainTextToken
            ]);
        }

        return $this->respondSuccess([
            'token-type' => 'basic-token',
            'token'      => auth()->user()->createToken('basic-token', ['basic'])->plainTextToken
        ]);
    }

    /**
     * Log user out
     *
     * @return App\Traits\ApiResponse
     */
    public function logout()
    {
        auth()->user()->tokens()->delete();

        return $this->respondSuccess('logged out');
    }
}
