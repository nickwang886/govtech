<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

/*
|--------------------------------------------------------------------------
| Api Responser Trait
|--------------------------------------------------------------------------
|
| This trait will be used for any response we sent to clients.
|
*/

trait ApiResponse
{
    /**
     * status code
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @param $data
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Gets the value of statusCode.
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param int $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondSuccess($message, $data = [])
    {
        return $this->respond([
            'message' => $message,
            'data'    => $data
        ]);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreated($message = '', $data = [])
    {
        $response = Response::HTTP_CREATED;

        return $this->setStatusCode($response)->respond(['message' => $message, 'data' => $data]);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'errors' => [["message" => $message] ?: $this->getStatusCode()]
        ]);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithErrors($message, $errors)
    {
        return $this->respond([
            'message' => $message,
            'errors'  => $errors
        ]);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondBadRequest($message = '', $errors = [])
    {
        $response = Response::HTTP_BAD_REQUEST;

        !$message ? $message = Response::$statusTexts[$response] : null;

        return $this->setStatusCode($response)->respondWithErrors($message, $errors);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound($message = '', $errors = [])
    {
        $response = Response::HTTP_NOT_FOUND;

        !$message ? $message = Response::$statusTexts[$response] : null;

        return $this->setStatusCode($response)->respondWithErrors($message, $errors);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondUnprocessableEntity($message = '')
    {
        $response = Response::HTTP_UNPROCESSABLE_ENTITY;

        !$message ? $message = Response::$statusTexts[$response] : null;

        return $this->setStatusCode($response)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondUnauthorized($message = '', $errors = [])
    {
        $response = Response::HTTP_UNAUTHORIZED;

        return $this->setStatusCode($response)->respond(['message' => $message, 'errors' => $errors]);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondForbidden($message = '', $errors = [])
    {
        $response = Response::HTTP_FORBIDDEN;

        return $this->setStatusCode($response)->respondWithErrors($message, $errors);
    }
}
